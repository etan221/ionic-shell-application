# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [2.0.0](https://bitbucket.org/uoa/ionic-shell-application/compare/v1.0.0...v2.0.0) (2020-08-14)

### ⚠ BREAKING CHANGES

- Upgrade angular core and cli to v10 and ionic/angular to v5.2.3 . Update auth library to v0.3.0 and error-pages library to v0.1.0 ([d453a8c](https://bitbucket.org/uoa/ionic-shell-application/commit/d453a8c829d37fe91d61bbfeb21519e9ee2286ee))
- Upgrade deprecated ngx-take-until-destroy library with @ngneat/until-destroy ([c2c6893](https://bitbucket.org/uoa/ionic-shell-application/commit/c2c6893fcddfbcb7f04c7c1146b3b26fc8530842))

## 1.0.0 (2020-07-07)

### Features

- Shell Application build with Ionic v5 and angular v8
- Auth library v0.2.0, package implementation
- Error library v0.0.7, package implementation
- UOA starter application with quick start header , footer, page layout and css
- Added meta service to add meta tag of description to pages
