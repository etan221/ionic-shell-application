UOA SPA Ionic shell-application is a template with default header , footer, css, Auth Library and Error Pages library. It also include MetaService to add/update meta description of page.

## Meta Service

Meta Service is implemented on home page.

## Auth

In order to integrate AWS based Single Sign On with this app please follow this link (https://wiki.auckland.ac.nz/pages/viewpage.action?pageId=173769037).

## Plugins to install

Install `Prettier - Code formatter` and enable format file on save in preferences

## Development server

Run `ng serve --port=8100` for a dev server. Navigate to `http://localhost:8100/`. Port 8100 is configured for Auth. If you try any other port you have to change Auth settings accordingly. The app will automatically reload if you change any of the source files.
or
Run `ionic serve` for a dev server. Navigate to `http://localhost:8100/`. The app will automatically reload if you change any of the source files.

## Code scaffoldinng

Run `ionic generate page page-name` to generate a new page. You can also use `ng generate component|directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Ionic CLI use `ionic help` or go check out the (https://ionicframework.com/)
To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
