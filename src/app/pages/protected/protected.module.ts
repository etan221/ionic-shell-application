import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HeaderModule, FooterModule, LoadingIndicatorModule } from 'src/app/shared';

import { ProtectedPageRoutingModule } from './protected-routing.module';
import { ProtectedPage } from './protected.page';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, HeaderModule, FooterModule, LoadingIndicatorModule, ProtectedPageRoutingModule],
  declarations: [ProtectedPage],
})
export class ProtectedPageModule {}
