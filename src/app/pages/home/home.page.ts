import { Component } from '@angular/core';
import { MetaService } from 'src/app/core/services';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  constructor(private _metaService: MetaService) {
    const metaDescription = `Explore the wide range of subjects available at the University of Auckland.`;
    this._metaService.addMetaToPage(metaDescription);
  }
}
