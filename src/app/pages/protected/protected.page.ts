import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { LoginService } from '@uoa/auth';
import { Observable, Subject } from 'rxjs';

@Component({
  selector: 'app-protected',
  templateUrl: './protected.page.html',
  styleUrls: ['./protected.page.scss'],
})
export class ProtectedPage {
  public helloWorld;
  public user$;
  public user;
  public loading$ = new Subject<boolean>();
  public loggedIn$: Observable<boolean>;
  constructor(private http: HttpClient, private _loginService: LoginService) {
    this.loggedIn$ = this._loginService.loggedIn$;
    this._loginService.isAuthenticated();
    this.user$ = this._loginService.userInfo$;
  }

  public getHelloWorld() {
    this.loading$.next(true);
    this.http.get('https://apigw.sandbox.amazon.auckland.ac.nz/hello-world').subscribe((res) => {
      this.helloWorld = res;
      this.loading$.next(false);
    });
  }

  public logout() {
    this._loginService.logout();
  }

  async getUserInfo() {
    this.loading$.next(true);
    this.user = await this._loginService.getUserInfo();
    this.loading$.next(false);
  }
}
